<?php

use Illuminate\Support\Facades\Route;
use Spatie\Analytics\Period;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


//Route::get('/', [App\Http\Controllers\HomeController::class, 'login'])->name('login');

Auth::routes(['register' => true, 'reset' => false]);

Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'dashboard'])->middleware('auth')->name('dashboard');
Route::get('/map', [App\Http\Controllers\MapController::class, 'index'])->middleware('auth')->name('map');
Route::get('/board', [App\Http\Controllers\BoardController::class, 'index'])->middleware('auth')->name('board');
Route::get('/getdata', [App\Http\Controllers\BoardController::class, 'getData'])->name('getdata');
Route::get('/getproperties', [App\Http\Controllers\BoardController::class, 'getProperties'])->name('getproperties');
Route::post('/setdata', [App\Http\Controllers\BoardController::class, 'setData'])->name('setdata');

Route::prefix('admin')->middleware(['isAdmin'])->group(function () {
    Route::get('/play', [App\Http\Controllers\AdminController::class, 'play'])->name('play');
    Route::get('/getdata', [App\Http\Controllers\AdminController::class, 'getData'])->name('getdata');
    Route::post('/setdata', [App\Http\Controllers\AdminController::class, 'setData'])->name('setdata');
    Route::get('/dashboard', [App\Http\Controllers\AdminController::class, 'index'])->name('admin-dashboard');
    Route::get('/properties', [App\Http\Controllers\AdminController::class, 'properties'])->name('properties');
//    Route::get('/analytics', [App\Http\Controllers\AnalyticsController::class, 'analytics'])->name('analytics');
    Route::get('/data', function () {
        $analyticsData = Analytics::fetchMostVisitedPages(Period::days(7));
        dd($analyticsData);
    });

    Route::prefix('users')->middleware(['isAdmin'])->group(function () {
        Route::get('/list', [App\Http\Controllers\UserController::class, 'list'])->name('listUsers');
        Route::get('/add', [App\Http\Controllers\UserController::class, 'add'])->name('addUsers');
        Route::post('/add', [App\Http\Controllers\UserController::class, 'postAddForm'])->name('addUsers');
        Route::get('/edit/{user}', [App\Http\Controllers\UserController::class, 'edit'])->name('editUsers');
        Route::post('/edit/{user}', [App\Http\Controllers\UserController::class, 'postEditForm'])->name('editUsers');
        Route::get('/delete/{user}', [App\Http\Controllers\UserController::class, 'delete'])->name('deleteUsers');

    });

    Route::prefix('questions')->middleware(['isAdmin'])->group(function () {
        Route::get('/list', [App\Http\Controllers\QuestionsController::class, 'index'])->name('questions');
        Route::get('/add', [App\Http\Controllers\QuestionsController::class, 'create'])->name('addQuestion');
        Route::post('/add', [App\Http\Controllers\QuestionsController::class, 'postCreateQuestionForm'])->name('addQuestion');
    });
});
