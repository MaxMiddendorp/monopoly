module.exports = {
    purge: [
        './resources/**/*.blade.php',
        './resources/**/*.js',
        './resources/**/*.vue',
    ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
        backgroundImage: {
            kaart:
                "url('https://www.etopia.nl/wp-content/uploads/2018/07/Screenshot-2017-12-20-at-14.35.44.png')",
            spelbord:
                "url('https://wetenschappelijk.mprog.nl/course/31%20Simulaties/05%20Inleiding/MonopolyBordInternationaal.jpg')",
        },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
