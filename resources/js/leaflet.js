import Axios from "axios";
import axios from "axios";

if (document.getElementById("map")) {
    var map = L.map('map').setView([52.381366, 4.636008], 15);
    var url = "https://staddag.nl/img/pins/";
//https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1Ijoid3NzbGZuc3RyIiwiYSI6ImNram12a3NyeDJobDQycW83cHB2NDlrMTgifQ.0ytGrX2xH6OpuBDb9XpciQ'
    }).addTo(map);

    axios.get('/getproperties')
        .then((response) => {
            response.data.forEach(function (prop) {
                if (prop.latitude && prop.longitude) {
                    var icon = L.icon({
                        iconUrl: url + prop.icon + '.svg',
                        iconSize: [50, 50],
                        iconAnchor: [25, 50],
                        popupAnchor: [0, -50],
                    });
                    L.marker([prop.latitude, prop.longitude], {icon: icon}).bindPopup(prop.name).addTo(map);
                }
            })
        });
}
