import './leaflet'

import Vue from 'vue';
import Board from './components/board';
import Play from './components/play';

Vue.component('admin-play', Play);
Vue.component('board', Board);

$.fn.vue = function () {
    $(this).find('.Vue').each(function () {
        new Vue({
            el: this,
        });
    });
};

// Give other scripts time to register components
setTimeout(() => {
    $(document).vue();
}, 1);
