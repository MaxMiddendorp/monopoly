@extends('layouts.app')

@section('content')
    <div class="w-full flex flex-wrap">
        <div class="md:w-1/3 flex flex-col sm:w-full">
            <div class="flex justify-center md:justify-start pt-12 md:pl-12 md:-mb-24">
            </div>
            <div class="flex flex-col justify-center md:justify-start my-auto pt-8 md:pt-0 px-8 md:px-24 lg:px-32">
                <p class="text-center text-3xl">Welcome to {{ config('app.name') }}</p>
                <form class="flex flex-col pt-3 md:pt-8" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="flex flex-col pt-4">
                        <label for="username" class="text-lg">{{ __('Username') }}</label>
                        <input name="username" type="text" id="username" placeholder="Team X"
                               class="w-full h-10 px-3 mb-2 text-base text-gray-700 placeholder-gray-600 border rounded-lg focus:shadow-outline @error('username') border-red-700 @enderror"
                               autocomplete="username">
                        @error('username')
                        <span class="invalid-feedback text-red-600" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="flex flex-col pt-4">
                        <label for="password" class="text-lg">{{ __('Password') }}</label>
                        <input name="password" type="password" id="password" placeholder="Password"
                               class="w-full h-10 px-3 mb-2 text-base text-gray-700 placeholder-gray-600 border rounded-lg focus:shadow-outline @error('password') border-red-700 @enderror"
                               autocomplete="password">
                        @error('password')
                        <span class="invalid-feedback text-red-600" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <button type="submit" class="bg-black text-white font-bold text-lg hover:bg-gray-700 p-2 mt-8">
                        {{ __('Login') }}
                    </button>
                </form>
                <br>
                <p class="text-center pb-4">&copy; {{ now()->year }} <a target="_blank" class="underline text-blue-600" href="https://gitlab.com/MaxMiddendorp">Max
                        Middendorp</a></p>
            </div>
        </div>

        <div class="md:w-2/3 shadow-2xl">
            <img class="object-cover w-full h-screen hidden md:block" src="{{ asset("img/game.jpg") }}">
        </div>
    </div>
@endsection

