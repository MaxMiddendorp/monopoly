@extends('layouts.app')

@section('content')
    {{--    <div class="w-full h-full flex flex-wrap">--}}
    <div class="flex flex-col sm:flex-row">
        <div class="w-full sm:w-1/2">
            <img
                class="h-full w-full bg-cover"
                src="{{ asset('img/homepage.jpg') }}"
                alt="Spelbord">
        </div>
        <div class="w-full sm:w-1/2 p-5 md:p-20">
            <h1 class="font-bold text-5xl text-center">Uitleg Levend Monopoly</h1>
            <br>
            <p class="text-left">Hoe werkt levend Monopoly? <br>
                We verzamelen op een centrale locatie, de Grote markt te Haarlem. Daar krijgt je de speluitleg, word je
                in teams verdeeld en krijg je je inloggegevens. Elk team moet proberen in real-life de verschillende
                straten en panden in Haarlem te veroveren. Net als in het echte spel is het de bedoeling zo snel
                mogelijk zo veel mogelijk bezit te hebben. Met de dobbelstenen op de verzamelplek kom je steeds op de
                volgende positie. Tijdens de game kom je langs de gevangenis en langs de kansplekken... Welke weg kiezen
                jij en je gezelschap?
                <br><br>
                Na iedere worp verplaatst jouw personage over het spelbord, bij elke plek volgt een opdracht die op
                locatie uitgevoerd dient te worden.
            </p>
        </div>
    </div>
@endsection

