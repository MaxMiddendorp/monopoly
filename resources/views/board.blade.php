@extends('layouts.app')

@section('scripts')
    <link href="{{ asset('css/spelbord.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="">
        <div class="Vue">
            <board :user_id="{{\Illuminate\Support\Facades\Auth::id()}}"/>
        </div>
    </div>
@endsection

