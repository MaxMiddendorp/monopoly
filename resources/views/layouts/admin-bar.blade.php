<aside class="flex flex-col items-center bg-white text-gray-700 shadow">
    <ul>
        <li class="hover:bg-gray-100">
            <a
                href="{{ route('listUsers') }}"
                class="h-16 px-6 flex flex justify-center items-center w-full
					focus:text-orange-500">
                <i class="fas fa-users"></i>
            </a>
        </li>
        <li class="hover:bg-gray-100">
            <a
                href="{{ route('play') }}"
                class="h-16 px-6 flex flex justify-center items-center w-full
					focus:text-orange-500">
                <i class="fas fa-home"></i>
            </a>
        </li>
        <li class="hover:bg-gray-100">
            <a
                href="{{ route('questions') }}"
                class="h-16 px-6 flex flex justify-center items-center w-full
					focus:text-orange-500">
                <i class="fas fa-question-circle"></i>
            </a>
        </li>
    </ul>
</aside>
