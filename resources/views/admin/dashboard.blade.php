@extends('layouts.app')

@section('content')
    <div class="flex flex-wrap h-full w-full">
        <div class="font-sans antialiased">
            <div class="w-screen h-full flex bg-gray-200">
{{--                @include('layouts.admin-bar')--}}
                <div class="w-full h-full p-4">
                    <h1 class="font-bold text-5xl text-center">
                        Welkom, {{ \Illuminate\Support\Facades\Auth::user()->username }}</h1>
                    <br>
                    <div class="flex flex-col md:flex-row mx-auto">
                        <div class="px-4">
                            <a
                                type="button"
                                class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                href="{{ route('listUsers') }}"
                            >
                                Users
                            </a>
                        </div>
                        <div class="px-4">
                            <a
                                type="button"
                                class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                href="{{ route('play') }}"
                            >
                                Game
                            </a>
                        </div>
                        <div class="px-4">
                            <a
                                type="button"
                                class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                href="{{ route('questions') }}"
                            >
                                Questions
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

