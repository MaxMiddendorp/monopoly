@extends('layouts.app')

@section('content')
    <div class="flex flex-wrap h-full w-full">
        <div class="font-sans antialiased">
            <div class="w-screen h-full flex bg-gray-200">
                {{--                @include('layouts.admin-bar')--}}
                <div class="w-full md:p-8 sm:p-4">
                    <div class="flex flex-col sm:flex-row">
                        <div class="w-full sm:w-1/2">
                            Vragen:
                            <br>
                            <a
                                type="button"
                                class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                href="{{ route('addQuestion') }}"
                            >
                                Add
                            </a>
                            <table class="text-left w-full border-collapse">
                                <thead>
                                <tr>
                                    <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light border-b border-black">
                                        Locatie
                                    </th>
                                    <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light border-b border-black">
                                        Vraag
                                    </th>
                                    <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light border-b border-black">
                                        Type
                                    </th>
{{--                                    <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light border-b border-black">--}}
{{--                                        Acties--}}
{{--                                    </th>--}}
{{--                                    <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light border-b border-black">--}}
{{--                                    </th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $question as $question )
                                    <tr>
                                        <td class="py-4 px-6 border-b border-grey-light">{{$question->property->name}}</td>
                                        <td class="py-4 px-6 border-b border-grey-light">{{$question->question}}</td>
                                        <td class="py-4 px-6 border-b border-grey-light">
                                            @switch($question->type)
                                                @case('text')
                                                {{ "📝" }}
                                                @break
                                                @case('photo')
                                                {{ "📷" }}
                                                @break
                                                @case('both')
                                                {{ "💡" }}
                                                @break
                                                @default
                                                {{ "🚫" }}
                                                @break;
                                            @endswitch

                                            {{--                                            {{ $question->type }}--}}
                                        </td>
                                        {{--                                        <td class="py-4 px-6 border-b border-grey-light">--}}
                                        {{--                                            <a--}}
                                        {{--                                                type="button"--}}
                                        {{--                                                class="cursor-pointer btn-primary transition duration-300 ease-in-out focus:outline-none focus:shadow-outline bg-purple-700 hover:bg-purple-900 text-white font-normal py-2 px-4 mr-1 rounded"--}}
                                        {{--                                                href="{{ route('editUser', ['user' => $user->id]) }}"--}}
                                        {{--                                            >--}}
                                        {{--                                                Bewerk--}}
                                        {{--                                            </a>--}}
                                        {{--                                        </td>--}}
                                        {{--                                        <td class="py-4 px-6 border-b border-grey-light">--}}
                                        {{--                                            <a--}}
                                        {{--                                                type="button"--}}
                                        {{--                                                class="cursor-pointer btn-primary transition duration-300 ease-in-out focus:outline-none focus:shadow-outline bg-purple-700 hover:bg-purple-900 text-white font-normal py-2 px-4 mr-1 rounded"--}}
                                        {{--                                                onclick="return confirm('Weet u zeker dat u deze gebruiker wilt deactiveren?')"--}}
                                        {{--                                                href="{{ route('deleteUser', [ 'user' => $user->id]) }}"--}}
                                        {{--                                            >--}}
                                        {{--                                                Verwijder--}}
                                        {{--                                            </a>--}}
                                        {{--                                        </td>--}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
