@extends('layouts.app')

@section('content')
    <div class="flex flewrap">
        <div class="font-sans antialiased">
            <div class="w-screen flex bg-gray-200">
                {{--                @include('layouts.admin-bar')--}}
                <div class="w-full">
                    <form method="POST" action="{{ route('addQuestion') }}" class="m-4">
                        @csrf
                        <div class="mt-4">
                            <label class="w-30" for="location">Locatie:</label>
                            {{--                            <input id="username" class="block mt-1 w-full" type="text"--}}
                            {{--                                     name="username" :value="old('username')" required--}}
                            {{--                                     autofocus/>--}}
                            <select id="location" name="property_id"
                                    class="h-10 pl-3 pr-6 text-base placeholder-gray-600 border rounded-lg appearance-none focus:shadow-outline">
                                @foreach(\App\Models\Property::all() as $property)
                                    <option value="{{$property->id}}">{{$property->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mt-4">
                            <label class="w-10" for="question">Vraag:</label>
                            <input id="question"
                                   class="h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline"
                                   type="text"
                                   name="question" required autocomplete="off"/>
                        </div>
                        <div class="mt-4">
                            <label class="w-30" for="Type">Soort:</label>
                            <select name="type"
                                    class="h-10 pl-3 pr-6 text-base placeholder-gray-600 border rounded-lg appearance-none focus:shadow-outline">
                                <option selected value="both">Beide</option>
                                <option value="text">Text</option>
                                <option value="photo">Foto</option>
                                <option value="unk">Onbekend</option>
                            </select>
                        </div>
                        <div class="flex items-center justify-end mt-4">
                            <button
                                class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                                {{ "Save" }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

