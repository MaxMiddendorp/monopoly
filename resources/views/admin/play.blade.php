@extends('layouts.app')

@section('content')
    <div class="flex flex-wrap h-full w-full">
        <div class="font-sans antialiased">
            <div class="w-screen h-full flex bg-gray-200">
{{--                @include('layouts.admin-bar')--}}
                <div class="Vue w-full md:p-8 sm:p-4">
                    <admin-play></admin-play>
                </div>
            </div>
        </div>
    </div>
@endsection
