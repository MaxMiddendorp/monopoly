@extends('layouts.app')

@section('content')
    <div class="h-full w-full mx-auto">
        <div class="font-sans antialiased">
            <div class="w-screen h-full flex bg-gray-200">
                {{--                @include('layouts.admin-bar')--}}
                <div class="w-full h-full p-6">
                    <a
                        type="button"
                        class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                        href="{{ route('addUsers') }}"
                    >
                        Add
                    </a>
                    <div class="bg-white shadow-md rounded my-6 overflow-x-scroll">
                        <table class="text-left w-full border-collapse">
                            <thead>
                            <tr>
                                <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                    Username
                                </th>
                                <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                    Is admin
                                </th>
                                <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                    Acties
                                </th>
                                <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach( $users as $user )
                                <tr>
                                    <td class="py-4 px-6 border-b border-grey-light">{{$user->username}}</td>
                                    <td class="py-4 px-6 border-b border-grey-light">{{$user->isAdmin}}</td>
                                    <td class="py-4 px-6 border-b border-grey-light">
                                        <a
                                            type="button"
                                            class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                            href="{{ route('editUsers', ['user' => $user->id]) }}">
                                            Bewerk
                                        </a>
                                    </td>
                                    <td class="py-4 px-6 border-b border-grey-light">
                                        <a
                                            type="button"
                                            class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                            onclick="return confirm('Weet u zeker dat u deze gebruiker wilt verwijderen?')"
                                            href="{{ route('deleteUsers', [ 'user' => $user->id]) }}">
                                            Verwijder
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

