@extends('layouts.app')

@section('content')
    <div class="flex flewrap h-full">
        <div class="font-sans antialiased">
            <div class="w-screen flex bg-gray-200">
{{--                @include('layouts.admin-bar')--}}
                <div>
                    <form method="POST" action="{{ route('addUsers') }}" class="m-4 w-full">
                        @csrf
                        <div class="text-gray-700 md:flex md:items-center">
                            <div class="mb-1 md:mb-0 md:w-1/3">
                                <label>Gebruikersnaam</label>
                            </div>
                            <div class="md:w-2/3 md:flex-grow">
                                <input id="username"
                                       class="w-full h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline"
                                       type="text"
                                       name="username" :value="old('username')" maxlength="15" required
                                       autofocus/>
                            </div>
                        </div>
                        <br>
                        <div class="text-gray-700 md:flex md:items-center">
                            <div class="mb-1 md:mb-0 md:w-1/3">
                                <label>Wachtwoord</label>
                            </div>
                            <div class="md:w-2/3 md:flex-grow">
                                <input id="password" minlength="8"
                                       class="w-full h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline"
                                       type="password"
                                       name="password" :value="old('password')" required/>
                            </div>
                        </div>
                        <br>
                        <div class="text-gray-700 md:flex md:items-center">
                            <div class="mb-1 md:mb-0 md:w-1/3 pr-12">
                                <label>Bevestig wachtwoord</label>
                            </div>
                            <div class="md:w-2/3 md:flex-grow">
                                <input id="password" minlength="8"
                                       class="w-full h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline"
                                       type="password"
                                       name="password_confirmation" :value="old('password')" required/>
                            </div>
                        </div>
                        <div class="flex items-center justify-end mt-4">
                            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                                {{ __('Opslaan') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

