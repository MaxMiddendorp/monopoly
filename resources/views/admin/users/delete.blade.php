@extends('layouts.app')

@section('content')
    <div class="flex flewrap">
        <div class="font-sans antialiased">
            <div class="w-screen flex bg-gray-200">
{{--                @include('layouts.admin-bar')--}}
                <div>
                    <p>Gebruiker {{ $user->name }} is successvol verwijderd</p>
                </div>
            </div>
        </div>
    </div>
@endsection

