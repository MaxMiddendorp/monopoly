@extends('layouts.app')

@section('content')
    <div class="flex flewrap">
        <div class="font-sans antialiased">
            <div class="w-screen flex bg-gray-200">
{{--                @include('layouts.admin-bar')--}}
                <div>
                    <form method="POST" action="{{ route('editUsers', ['user' => $user->id]) }}" class="m-4">
                        @csrf
                        <div class="mt-4">
                            <label for="username" :value="__('Username')"/>
                            <input id="username" class="block mt-1 w-full" type="text"
                                   name="username" value="{{$user->username}}"
                                   required
                                   autofocus/>
                        </div>
                        <div class="mt-4">
                            <label for="password" :value="__('Wachtwoord')"/>
                            <input id="password" class="block mt-1 w-full" type="password"
                                   name="password"
                            />
                        </div>
                        <div class="flex items-center justify-end mt-4">
                            <button class="ml-4">
                                {{ __('Opslaan') }}
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

