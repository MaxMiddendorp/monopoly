<?php

namespace App\Http\Controllers;

use App\Models\Question;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
    public function index()
    {
        $question = Question::all();
//        dd($question);
        return view('admin.questions.list', ['question' => $question]);
    }

    public function create()
    {
        return view('admin.questions.add');
    }

    public function postCreateQuestionForm(Request $request)
    {
        $question = new Question($request->all());
        $question->save();
        return view('admin.questions.add');

    }

    public function store(Request $request)
    {
        //
    }

    public function show(Question $questions)
    {
        //
    }

    public function edit(Question $questions)
    {
        return view('admin.questions.edit', [
            'questions' => $questions
        ]);
    }

    public function postEditQuestionForm(Request $request)
    {
        $question = new Question($request->all());
        $question->save();
        return view('admin.questions.list');
    }

    public function update(Request $request, Question $questions)
    {
        //
    }

    public function destroy(Question $questions)
    {
        //
    }
}
