<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Game;
use App\Models\Player;
use App\Models\Property;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use ReflectionClass;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.dashboard');
    }

    public function play()
    {
        return view('admin.play');
    }

    public function getData()
    {
        if (Game::get()->last() === null)
            Game::create([]);
        $users = User::all();
        $game = Game::with('Players', 'Players.User', 'Players.Answers', 'Players.correctAnswers', 'Players.Answers.Question', 'Players.Answers.Question.Property')->get()->last();
        $places = Property::all();
        return ['users' => $users, 'game' => $game, 'places' => $places];
    }

    public function setData(Request $request)
    {
        switch ($request->input('type')) {
            case 'setPlayerPosition':
                $player = Player::find($request->input('playerID'));
                $player->property_id = $request->input('value');
                $player->save();
                $question = $player->property->questions->shuffle()->first();
                if ($question !== null) {
                    $question->answer()->create([
                        'player_id' => $player->id
                    ]);
                }
                break;
            case 'setUserActive':
                $user = User::find($request->input('userID'));
                $user->isActive = $request->input('value');
                $user->save();
                break;
            case 'makeNewGame':
                $game = Game::create([]);
                $game->save();
                break;
            case 'startGame':
                $game = Game::where('status', '!=', 'finished')->get()->last();
                $game->status = 'started';
                foreach (User::where('isActive', '=', 1)->get() as $player) {
                    $game->Players()->create(['user_id' => $player->id, 'other_position' => 'null', 'property_id' => Property::where('name', 'GO')->first()->id]);
                }
                $game->save();
                break;
            case 'endGame':
                $game = Game::where('status', '!=', 'finished')->get()->last();
                $game->status = 'finished';
                $game->save();
                break;
            case 'acceptLocation':
                $answer = Answer::find($request->answerID);
                $answer->location_confirm = filter_var($request->value, FILTER_VALIDATE_BOOLEAN);
                $answer->save();
                break;
            case 'acceptAnswer':
                $value = filter_var($request->value, FILTER_VALIDATE_BOOLEAN);
                $answer = Answer::find($request->answerID);
                if ($value === true) {
                    $answer->question->property->player_id = $answer->player_id;
                    $answer->question->property->save();
                    $answer->delete();
                } else {
                    $answer->answer = null;
                    $answer->delete();
                }
                $answer->save();
                break;
        }
        return [];
    }
}
