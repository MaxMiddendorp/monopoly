<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function list()
    {
        $users = User::all();
        return view('admin.users.list', ['users' => $users]);
    }

    public function add()
    {
        return view('admin.users.add');
    }

    public function postAddForm(Request $request)
    {
        $request->validate([
            'username' => 'required|string|max:255',
            'password' => 'required|string|confirmed|min:6',
        ]);
        $user = new User($request->all());
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect()->route('listUsers');
    }

    public function edit(User $user)
    {
        return view('admin.users.edit', [
            'user' => $user
        ]);
    }

    public function postEditForm(Request $request, User $user)
    {
        dd($request);
//        $request->validate([
//            'username' => 'required|string|max:255',
//            'password' => 'required|string|min:6',
//        ]);
        if ($request['password'] != null) {
            $user->password = Hash::make($request->input('password'));
        }
        $user->username = $request->username;
        $user->update();
        return redirect()->route('listUsers');
    }

    public function delete(User $user)
    {
        $user->delete();
            return redirect()->route('listUsers');
    }
}
