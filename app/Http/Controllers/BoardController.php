<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Game;
use App\Models\Property;
use App\Models\Street;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use League\CommonMark\Inline\Element\Image;

class BoardController extends Controller
{
    public function index()
    {
        return view('board');
    }

    public function getProperties()
    {
        $places = Property::all();
        return $places;
    }

    public function getData()
    {
        $game = Game::with('Players', 'Players.Answers', 'Players.Answers.Question')->get();
        $places = Street::with('Properties', 'Properties.active_players', 'Properties.active_players.User', 'Properties.Player.User')->get();
        if ($game !== null)
            $game = $game->last();
        return ['game' => $game, 'places' => $places];
    }

    public function setData(Request $request)
    {
        switch ($request->input('type')) {
            case 'uploadLocation':
                if ($request->hasFile('photo')) {
                    $request->photo->store('game/' . Game::all()->last()->id, 'public');

                    $answer = Answer::find($request->answerID);
                    $answer->location_photo = 'game/' . Game::all()->last()->id . '/' . $request->photo->hashName();
                    $answer->location_confirm = null;
                    $answer->save();
                }
                break;
            case 'uploadAnswer':
                if ($request->hasFile('photo')) {
                    $request->photo->store('game/' . Game::all()->last()->id, 'public');

                    $answer = Answer::find($request->answerID);
                    $answer->answer = 'game/' . Game::all()->last()->id . '/' . $request->photo->hashName();
                    $answer->save();
                }else{
                    $answer = Answer::find($request->answerID);
                    $answer->answer = $request->text;
                    $answer->save();
                }
                break;
        }
        return [];
    }
}
