<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $fillable = [
        'property_id',
        'question',
        'type',
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function answer()
    {
        return $this->hasMany(Answer::class);
    }
}
