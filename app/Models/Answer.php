<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Psy\Util\Str;

class Answer extends Model
{
    use HasFactory, SoftDeletes;

    public $guarded = [];

    protected $appends = ['location_photo_path', 'answer_path'];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function getLocationPhotoPathAttribute()
    {
        return url('storage/'.$this->location_photo);
    }

    public function getAnswerPathAttribute()
    {
        if (substr($this->answer, 0,5) == 'game/') {
            return url('storage/' . $this->answer);
        }else{
            return false;
        }
    }
}
