<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function property(){
        return $this->belongsTo(Property::class);
    }

    public function answers(){
        return $this->hasMany(Answer::class);
    }

    public function correctAnswers(){
        return $this->hasMany(Answer::class)->onlyTrashed()->whereNotNull('answer');
    }
}
