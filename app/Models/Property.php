<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;

    public function street()
    {
        return $this->belongsTo(Street::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function players()
    {
        return $this->hasMany(Player::class);
    }

    public function player(){
        return $this->belongsTo(Player::class);
    }

    public function active_players()
    {
        return $this->players()->where('game_id' , Game::all()->last()->id);
    }
}
